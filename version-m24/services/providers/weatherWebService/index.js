var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var request = require('request');

var portNumber = 3002;
var app = express()
app.use(bodyParser.json());

//example of pos request: { "lon": "18.035341", "lat": "59.315506"}
//if the response is wrong you get -100 in all the fields
app.post('/getWeather', function(req, res) {
    console.log("restarted");
    console.log(req.body);
    var longitude = req.body.weatherInformationRequest.lon;
    var lattitude = req.body.weatherInformationRequest.lat;
    request({
        url: 'http://api.trafikinfo.trafikverket.se/v1.1/data.json',
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'text/xml'
        }],
        body: "<REQUEST><LOGIN authenticationkey='f3c1bde2360a47bea3576b8274ce8b28'/><QUERY objecttype='WeatherStation'>" +
            "<FILTER><WITHIN name='Geometry.WGS84' shape='center' value='" + longitude + " " + lattitude + "' radius='7000m' /></FILTER>" +
            "<INCLUDE>Measurement.Air.Temp</INCLUDE>" +
            "<INCLUDE>Measurement.MeasureTime</INCLUDE>" +
            "<INCLUDE>Measurement.Road.Temp</INCLUDE>" +
            "<INCLUDE>Measurement.Air.RelativeHumidity</INCLUDE>" +
            "<INCLUDE>Measurement.Wind</INCLUDE>" +
            "<INCLUDE>Active</INCLUDE>" +
            "<INCLUDE>Name</INCLUDE>" +
            "<INCLUDE>Active</INCLUDE>" +
            "<INCLUDE>Name</INCLUDE>" +
            "</QUERY></REQUEST>"
    }, function(error, response, body) {
        var responseContent = JSON.parse(response.body).RESPONSE.RESULT;
        if (responseContent != undefined && Array.isArray(responseContent) && Array.isArray(responseContent[0].WeatherStation) && responseContent[0] != undefined && responseContent[0].WeatherStation[0] != undefined) {
            console.log('airTemperature');
            console.log(responseContent[0].WeatherStation[0].Measurement.Road.Temp);
            res.json({
                weatherInformationResponse: {
                    roadTemperature: responseContent[0].WeatherStation[0].Measurement.Road.Temp,
                    airTemperature: responseContent[0].WeatherStation[0].Measurement.Air.Temp,
                    airRelativeHumidity: responseContent[0].WeatherStation[0].Measurement.Air.RelativeHumidity,
                    windForce: responseContent[0].WeatherStation[0].Measurement.Wind.Force
                }
            });
        } else {            
            console.log('false');
            console.log(-100);
            res.json({
                weatherInformationResponse: {
                    roadTemperature: -100,
                    airTemperature: -100,
                    airRelativeHumidity: -100,
                    windForce: -100
                }
            });
        }

    });
});

app.listen(portNumber);
console.log('server is listening on port ' + portNumber);
