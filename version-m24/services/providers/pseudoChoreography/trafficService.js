const async = require('async');
const request = require('request');
var converters = require('./converters');

function httpPostCongestion(route, callback) {
    const options = {
        url: "http://jinx.viktoria.chalmers.se:3004/getCongestion",
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'application/json'
        }],
        json: route
    };
    request(options,
        function(err, res, body) {
            callback(err, body);
        }
    );
}

function httpPostAccidents(waypoint, callback) {
    const options = {
        url: "http://jinx.viktoria.chalmers.se:3003/getAccidents",
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'application/json'
        }],
        json: waypoint
    };
    request(options,
        function(err, res, body) {
            callback(err, body);
        }
    );
}

function httpPostWeather(waypoint, callback) {
    const options = {
        url: "http://jinx.viktoria.chalmers.se:3002/getWeather",
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'application/json'
        }],
        json: waypoint
    };
    request(options,
        function(err, res, body) {
            callback(err, body);
        }
    );
}

var createAccidentRequestArray = function(congestionResponse) {
    var accidentRequests = [];
    converters.congestionStatusResponseToSEARPResponse(congestionResponse).forEach(function(element, routeIndex) {
        element.ecoSpeedRouteSegmentsInformation.forEach(function(segment) {
            var accidentRequest = {
                accidentInformationRequest: {
                    lon: segment.routeSegment.start.lon,
                    lat: segment.routeSegment.start.lat
                }
            }
            accidentRequests.push(accidentRequest);
        })
    });
    return accidentRequests;
}

var createWeatherRequestArray = function(congestionResponse) {
    var weatherRequests = [];
    converters.congestionStatusResponseToSEARPResponse(congestionResponse).forEach(function(element, routeIndex) {
        element.ecoSpeedRouteSegmentsInformation.forEach(function(segment) {
            var weatherRequest = {
                weatherInformationRequest: {
                    lon: segment.routeSegment.start.lon,
                    lat: segment.routeSegment.start.lat
                }
            }
            weatherRequests.push(weatherRequest);
        })
    });
    return weatherRequests;
}

var combineAccidentsAndRoutes = function(routes, accidents) { //it is important to iterate through the array the same way we did forming accidentRequests
    var accidentIndex = 0;
    routes.forEach(function(route, routeIndex) {
        route.ecoSpeedRouteSegmentsInformation.forEach(function(segmentObject, segmentIndex) {
            routes[routeIndex].ecoSpeedRouteSegmentsInformation[segmentIndex].extraDataWaypoints = 
            	converters.accidentsResponseArrayToExtraDataWaypointsInfo(accidents[accidentIndex].accidentInformationResponse.accidents);
            //Here some additional trasformation might be needed in order to satisfy wsdl
            accidentIndex++;
        });
    });
    return routes;
}

var combineWeatherAndRoutes = function(routes, weatherData) { //it is important to iterate through the array the same way we did forming weatherRequests
    var weatherDataIndex = 0;
    routes.forEach(function(route, routeIndex) {
        route.ecoSpeedRouteSegmentsInformation.forEach(function(segmentObject, segmentIndex) {
            routes[routeIndex].ecoSpeedRouteSegmentsInformation[segmentIndex].weatherCondition = weatherData[weatherDataIndex].weatherInformationResponse;
            //Here some additional trasformation might be needed in order to satisfy wsdl
            weatherDataIndex++;
        });
    });
    return routes;
}


var getCongestions = function(congestionRequests, response) {
    var responseRoutes = [];
    var res1 = {};
    async.map(congestionRequests, httpPostCongestion, function(err, res) {
        if (err) return console.log(err);
        res1 = res;
        responseRoutes = converters.congestionStatusResponseToSEARPResponse(res);
        var segmentStartingPoints = createAccidentRequestArray(res);
        async.map(segmentStartingPoints, httpPostAccidents, function(err, res) {
            if (err) return console.log(err);
            responseRoutes = combineAccidentsAndRoutes(responseRoutes, res);
            async.map(createWeatherRequestArray(res1), httpPostWeather, function(err, res) {
                if (err) return console.log(err);
                var  ecoRoutesResponse = {};
                responseRoutes = combineWeatherAndRoutes(responseRoutes, res);
                ecoRoutesResponse.ecoSpeedRoutesInformation = responseRoutes;
                response.json({
                	ecoRoutesResponseReturnType: {
                		choreographyId: {
            				choreographyId: 19716746767575
        				},
        				ecoRoutesResponse: ecoRoutesResponse
                	}
                });
            });
        });
    });
}

module.exports.getCongestions = getCongestions;
