var async = require('async'); //waterfall';
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var request = require('request');
var parseString = require('xml2js').parseString;

var portNumber = 3001;
var app = express()
app.use(bodyParser.json());

app.post('/bridgeNextClosure', function(req, res) {
    // example of post request body {"origin":{"lattitude":"57.709359", "longitude":"11.971648"},  destination":{"lattitude":"57.709359", "longitude":"11.971648"}}
    // so far we don't check if the bridge is included in the routeSegment we received.

    request({
        url: 'http://data.goteborg.se/BridgeService/v1.0/GetGABOpenedStatus/152d47b0-754b-4718-a635-6a7db1374572?format=json',
        method: 'GET'
    }, function(error, response, body) {
        var isBridgeClosed = JSON.parse(response.body).Value;
        var currentTime = new Date();
        var nextOpenedTime;
        var nextClosureFrom;
        var nextClosureTo;

        if (isBridgeClosed) {
            nextOpenedTime = currentTime.getTime() + 7 * 60000; // we estimate that the bridge is closed for 7 minutes more in case that it is already closed
        } else {
            nextOpenedTime = currentTime.getTime(); //+1-1 is transformation to int
        }

        var currentHour = Math.floor(currentTime.getTime() / 3600000) % 24 + 2; //hour of the day, +1 because of timezone adjustment
        var currentMinute = Math.floor(currentTime.getTime() / 60000) % 60; //minute of the hour
        var currentDayInMillis = Math.floor(currentTime.getTime() / (3600000 * 24)) * (3600000 * 24);

        if ((currentHour >= 9) && (currentHour < 18)) {
            nextClosureFrom = currentDayInMillis + (3600000 * 18);
            nextClosureTo = currentDayInMillis + 3600000 * 18 + 60000 * 10; // closure time from 18.00 to 18.10
        }

        if ((currentHour >= 0) && (currentHour < 9)) {
            nextClosureFrom = currentDayInMillis + (3600000 * 9);
            nextClosureTo = currentDayInMillis + 3600000 * 9 + 60000 * 10; // closure time from 9.00 to 9.10
        }

        if ((currentHour >= 18) && (currentHour < 24)) {
            nextClosureFrom = currentDayInMillis + (3600000 * (24 + 9));
            nextClosureTo = currentDayInMillis + (3600000 * (24 + 9)) + 60000 * 10; // closure time from 9.00 to 9.10 next day
        }

        res.json({
            closureStatus: { isClosed: isBridgeClosed, opensAtTimeMillis: nextOpenedTime },
            nextClosure: { fromMillis: nextClosureFrom, toMillis: nextClosureTo }
        });
    });
});

app.post('/getWeather', function(req, res) {
    // takes gps coordinates, example of post request body {"lon" : "14.75746321493", "lat":"14.75746321493"} 
    var longitude = req.body.lon;
    var lattitude = req.body.lat;
    request({
        url: 'http://api.trafikinfo.trafikverket.se/v1.1/data.json',
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'text/xml'
        }],
        body: "<REQUEST><LOGIN authenticationkey='f3c1bde2360a47bea3576b8274ce8b28'/><QUERY objecttype='WeatherStation'>" +
            "<FILTER><WITHIN name='Geometry.WGS84' shape='center' value='" + longitude + " " + lattitude + "' radius='7000m' /></FILTER>" +
            "<INCLUDE>Measurement.Air.Temp</INCLUDE>" +
            "<INCLUDE>Measurement.MeasureTime</INCLUDE>" +
            "<INCLUDE>Measurement.Road.Temp</INCLUDE>" +
            "<INCLUDE>Measurement.Air.RelativeHumidity</INCLUDE>" +
            "<INCLUDE>Measurement.Wind</INCLUDE>" +
            "<INCLUDE>Active</INCLUDE>" +
            "<INCLUDE>Name</INCLUDE>" +
            "<INCLUDE>Active</INCLUDE>" +
            "<INCLUDE>Name</INCLUDE>" +
            "</QUERY></REQUEST>"
    }, function(error, response, body) {
        var responseContent = JSON.parse(response.body).RESPONSE.RESULT;
        //TODO check for empty response
        res.json({
            roadTemperature: responseContent[0].WeatherStation[0].Measurement.Road.Temp,
            airTemperature: responseContent[0].WeatherStation[0].Measurement.Air.Temp,
            airRelativeHumidity: responseContent[0].WeatherStation[0].Measurement.Air.RelativeHumidity,
            windForce: responseContent[0].WeatherStation[0].Measurement.Wind.Force
        });
    });
});

app.post('/getAccidents', function(req, res) {
    // takes bridgeId, example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 
    console.log("restarted");
    console.log(req.body);
    var longitude = req.body.lon;
    var lattitude = req.body.lat;
    request({
        url: 'http://api.trafikinfo.trafikverket.se/v1.1/data.json',
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'text/xml'
        }],
        body: "<REQUEST><LOGIN authenticationkey='f3c1bde2360a47bea3576b8274ce8b28'/><QUERY objecttype='Situation'>" +
            "<FILTER><WITHIN name='Deviation.Geometry.WGS84' shape='center' value='" + longitude + " " + lattitude + "' radius='500m' /></FILTER>" +
            "<GTE name='Deviation.EndTime' value='$now'/>" +
            "</QUERY></REQUEST>"
    }, function(error, response, body) {
        console.log("returned");
        console.log(JSON.parse(response.body).RESPONSE);
        var responseContent = JSON.parse(response.body).RESPONSE.RESULT[0].Situation;
        //TODO check for empty response
        var accidents = [];
        responseContent.forEach(function(element, index) {
            accidents[index] = {};
            var re = new RegExp(" \\(*|\\)");
            console.log(element.Deviation[0].Geometry);
            var coordinates = element.Deviation[0].Geometry.WGS84.split(re);
            console.log(coordinates);
            accidents[index].longitude = parseFloat(coordinates[1]);
            accidents[index].lattitude = parseFloat(coordinates[2]);
            accidents[index].messageCodeValue = element.Deviation[0].MessageCodeValue;
            accidents[index].messageTypeValue = element.Deviation[0].MessageTypeValue;
            accidents[index].severityCode = element.Deviation[0].SeverityCode;
            accidents[index].description = element.Deviation[0].Message;
        })
        res.json({ accidents: accidents });
    });
});


app.post('/getCongestion', function(req, res) {
    console.log(req.body);
    var startLongitude = req.body.start.lon;
    var startLattitude = req.body.start.lat;
    var endLongitude = req.body.end.lon;
    var endLattitude = req.body.end.lat;

    var rp = require('request-promise');
    var url1 = {
        url: 'https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/xml?key=v9byd7y3x9dwgjhzyg36s4pa&point=' + startLattitude + ',' + startLongitude + '&unit=KMPH',
        method: 'GET',
        headers: [{
            name: 'content-type',
            value: 'application/json'
        }]
    };
    var url2 = {
        url: 'https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/xml?key=v9byd7y3x9dwgjhzyg36s4pa&point=' + endLattitude + ',' + endLongitude + '&unit=KMPH',
        method: 'GET',
        headers: [{
            name: 'content-type',
            value: 'application/json'
        }]
    };

    rp(url1)
    .then(response => {
            console.log('req1 ');
            console.log(response);
            parseString(response, function(err, result) {
                console.dir(result);
                console.log('Done');
            });
            return rp(url2);
        })
        .then(response => {
            console.log('req2 ');
            //console.log(response.body);
            parseString(response, function(err, result) {
                console.dir(result);
                console.log('Done');
            });
        })
        .catch(err => console.log);
});

function isRouteContainsBridge(origin, destination) {
    if ((isIncluded(origin, { topLeft: { lat: 57.720931, lon: 11.956240 }, bottomRight: { lat: 57.715952, lon: 11.966634 } }) &&
            isIncluded(destination, { topLeft: { lat: 57.710427, lon: 11.969474 }, bottomRight: { lat: 57.709359, lon: 11.971648 } })) ||
        (isIncluded(destination, { topLeft: { lat: 57.720931, lon: 11.956240 }, bottomRight: { lat: 57.715952, lon: 11.966634 } }) &&
            isIncluded(origin, { topLeft: { lat: 57.710427, lon: 11.969474 }, bottomRight: { lat: 57.709359, lon: 11.971648 } }))) {
        return true;
    }
    return false;
}

function isIncluded(point, area) {
    if (((point.lattitude < area.topLeft.lat) && (point.lattitude > area.bottomRight.lat)) &&
        ((point.longitude > area.topLeft.lon) && (point.lattitude < area.bottomRight.lon))) {
        return true;
    }
    return false;
}

app.listen(portNumber);
console.log('server is listening on port ' + portNumber);
