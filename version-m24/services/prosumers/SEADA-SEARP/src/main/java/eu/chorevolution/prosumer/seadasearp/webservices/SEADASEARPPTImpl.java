package eu.chorevolution.prosumer.seadasearp.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.prosumer.seadasearp.SEADASEARPPT;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesRequestType;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesRequestReturnType;
import eu.chorevolution.prosumer.seadasearp.EcoSpeedRoutesInformationResponseType;
import eu.chorevolution.prosumer.seadasearp.EcoSpeedRoutesInformationResponseReturnType;
import eu.chorevolution.prosumer.seadasearp.SendRequestType;
import eu.chorevolution.prosumer.seadasearp.RoutesRequestReturnType;
import eu.chorevolution.prosumer.seadasearp.RoutesRequest;
import eu.chorevolution.prosumer.seadasearp.SendRequestType;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesResponseReturnType;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesResponse;
import eu.chorevolution.prosumer.seadasearp.SendRequestType;
import eu.chorevolution.prosumer.seadasearp.EcoSpeedRoutesInformationRequestReturnType;
import eu.chorevolution.prosumer.seadasearp.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.prosumer.seadasearp.ReceiveRoutesSuggestionType;
import eu.chorevolution.prosumer.seadasearp.ReceiveRoutesSuggestionReturnType;

import eu.chorevolution.prosumer.seadasearp.business.ChoreographyDataService;
import eu.chorevolution.prosumer.seadasearp.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.prosumer.seadasearp.business.SEADASEARPService;


@Component(value="SEADASEARPPTImpl")
public class SEADASEARPPTImpl implements SEADASEARPPT {
	
	private static Logger logger = LoggerFactory.getLogger(SEADASEARPPTImpl.class);

	@Autowired
	private SEADASEARPService service;
	
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
    @Override
    public EcoRoutesRequestReturnType getEcoRoutes(EcoRoutesRequestType parameters) {
    	logger.info("CALLED getEcoRoutes ON SEADA-SEARP");	
    	EcoRoutesRequestReturnType result = new EcoRoutesRequestReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.getEcoRoutes(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public EcoSpeedRoutesInformationResponseReturnType setEcoSpeedRoutesInformation(EcoSpeedRoutesInformationResponseType parameters) {
    	logger.info("CALLED setEcoSpeedRoutesInformation ON SEADA-SEARP");	
    	EcoSpeedRoutesInformationResponseReturnType result = new EcoSpeedRoutesInformationResponseReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.setEcoSpeedRoutesInformation(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public RoutesRequestReturnType sendRoutesRequest(SendRequestType parameters) {
    	logger.info("CALLED sendRoutesRequest ON SEADA-SEARP");	
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		RoutesRequest businessResult = service.createRoutesRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		RoutesRequestReturnType result = new RoutesRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public EcoRoutesResponseReturnType sendEcoRoutesResponse(SendRequestType parameters) {
    	logger.info("CALLED sendEcoRoutesResponse ON SEADA-SEARP");	
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoRoutesResponse businessResult = service.createEcoRoutesResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		EcoRoutesResponseReturnType result = new EcoRoutesResponseReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public EcoSpeedRoutesInformationRequestReturnType sendEcoSpeedRoutesInformationRequest(SendRequestType parameters) {
    	logger.info("CALLED sendEcoSpeedRoutesInformationRequest ON SEADA-SEARP");	
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoSpeedRoutesInformationRequest businessResult = service.createEcoSpeedRoutesInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		EcoSpeedRoutesInformationRequestReturnType result = new EcoSpeedRoutesInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
	@Override
	public ReceiveRoutesSuggestionReturnType receiveRoutesSuggestion(ReceiveRoutesSuggestionType parameters) {
		logger.info("CALLED receiveRoutesSuggestion ON SEADA-SEARP");			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveRoutesSuggestion(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveRoutesSuggestionReturnType();
	}
     

	
}
