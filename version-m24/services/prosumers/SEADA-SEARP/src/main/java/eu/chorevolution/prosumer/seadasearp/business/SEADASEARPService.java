package eu.chorevolution.prosumer.seadasearp.business;

import eu.chorevolution.prosumer.seadasearp.EcoRoutesRequest;
import eu.chorevolution.prosumer.seadasearp.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.prosumer.seadasearp.RoutesRequest;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesResponse;
import eu.chorevolution.prosumer.seadasearp.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.prosumer.seadasearp.RoutesSuggestion;

public interface SEADASEARPService {

	void getEcoRoutes(EcoRoutesRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	void setEcoSpeedRoutesInformation(EcoSpeedRoutesInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
	      
    RoutesRequest createRoutesRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
    EcoRoutesResponse createEcoRoutesResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
    EcoSpeedRoutesInformationRequest createEcoSpeedRoutesInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
    void receiveRoutesSuggestion(RoutesSuggestion parameter, String choreographyTaskName, String senderParticipantName);
    
}
