package eu.chorevolution.prosumer.seadaseatsa.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationRequestReturnType;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationRequestType;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationResponseReturnType;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequestReturnType;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequestType;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationResponseReturnType;
import eu.chorevolution.prosumer.seadaseatsa.SEADASEATSAPT;
import eu.chorevolution.prosumer.seadaseatsa.SendRequestType;
import eu.chorevolution.prosumer.seadaseatsa.SendRequestTypeWithLoop;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationRequestReturnType;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationResponseReturnType;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationResponseType;
import eu.chorevolution.prosumer.seadaseatsa.business.ChoreographyDataService;
import eu.chorevolution.prosumer.seadaseatsa.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.prosumer.seadaseatsa.business.SEADASEATSAService;
import eu.chorevolution.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;


@Component(value="SEADASEATSAPTImpl")
public class SEADASEATSAPTImpl implements SEADASEATSAPT {
	
	private static Logger logger = LoggerFactory.getLogger(SEADASEATSAPTImpl.class);

	@Autowired
	private SEADASEATSAService service;
	
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
    @Override
    public EcoSpeedRouteInformationRequestReturnType getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequestType parameters) {
    	logger.info("CALLED getEcoSpeedRouteInformation ON SEADA-SEATSA");	
    	EcoSpeedRouteInformationRequestReturnType result = new EcoSpeedRouteInformationRequestReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.getEcoSpeedRouteInformation(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public EcoSpeedRoutesInformationRequestReturnType getEcoSpeedRoutesInformation(EcoSpeedRoutesInformationRequestType parameters) {
    	logger.info("CALLED getEcoSpeedRoutesInformation ON SEADA-SEATSA");	
    	EcoSpeedRoutesInformationRequestReturnType result = new EcoSpeedRoutesInformationRequestReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.getEcoSpeedRoutesInformation(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public TrafficRouteInformationResponseReturnType setRouteTrafficInformation(TrafficRouteInformationResponseType parameters) {
    	logger.info("CALLED setRouteTrafficInformation ON SEADA-SEATSA");	
    	TrafficRouteInformationResponseReturnType result = new TrafficRouteInformationResponseReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.setRouteTrafficInformation(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public EcoSpeedRouteInformationResponseReturnType sendEcoSpeedRouteInformationResponse(SendRequestType parameters) {
    	logger.info("CALLED sendEcoSpeedRouteInformationResponse ON SEADA-SEATSA");	
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoSpeedRouteInformationResponse businessResult = service.createEcoSpeedRouteInformationResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		EcoSpeedRouteInformationResponseReturnType result = new EcoSpeedRouteInformationResponseReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public EcoSpeedRoutesInformationResponseReturnType sendEcoSpeedRoutesInformationResponse(SendRequestType parameters) {
    	logger.info("CALLED sendEcoSpeedRoutesInformationResponse ON SEADA-SEATSA");	
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		EcoSpeedRoutesInformationResponse businessResult = service.createEcoSpeedRoutesInformationResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		EcoSpeedRoutesInformationResponseReturnType result = new EcoSpeedRoutesInformationResponseReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public TrafficRouteInformationRequestReturnType sendTrafficRouteInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendTrafficRouteInformationRequest ON SEADA-SEATSA");	
    	logger.info("sendTrafficRouteInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendTrafficRouteInformationRequest - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		TrafficRouteInformationRequest businessResult = service.createTrafficRouteInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		TrafficRouteInformationRequestReturnType result = new TrafficRouteInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }


	
}
