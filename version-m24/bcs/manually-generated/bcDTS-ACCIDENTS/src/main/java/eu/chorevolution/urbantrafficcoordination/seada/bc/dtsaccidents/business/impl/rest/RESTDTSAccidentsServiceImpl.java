package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.DTSAccidentsService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers.AccidentInformationRequestWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers.AccidentInformationResponseWrapper;

@Service
public class RESTDTSAccidentsServiceImpl implements DTSAccidentsService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSAccidentsServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;
	
	@Value("#{cfgproperties.path}")
	private String path;
	
	@Override
	public AccidentInformationResponse getWaypointAccidentInformation(AccidentInformationRequest accidentInformationRequest) {

		logger.info("CALLED getWaypointAccidentInformation ON REST bcDTS-ACCIDENTS");	
		AccidentInformationResponse response = null;	
		try {		
			Gson gson = new GsonBuilder().create();			
			HttpClient httpClient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();
			HttpPost postRequest = new HttpPost(uri);
			AccidentInformationRequestWrapper accidentInformationRequestWrapper = new AccidentInformationRequestWrapper();
			accidentInformationRequestWrapper.setAccidentInformationRequest(accidentInformationRequest);
			StringEntity requestEntity = new StringEntity(gson.toJson(accidentInformationRequestWrapper), ContentType.APPLICATION_JSON);			
			postRequest.setEntity(requestEntity);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String json = EntityUtils.toString(httpResponse.getEntity());
			AccidentInformationResponseWrapper accidentInformationResponseWrapper = gson.fromJson(json, AccidentInformationResponseWrapper.class);			
			response = accidentInformationResponseWrapper.getAccidentInformationResponse();
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}
		return response;
	}
	
}
