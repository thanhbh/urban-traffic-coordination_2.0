package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationRequest;

public class BridgeStatusInformationRequestWrapper {

	private BridgeStatusInformationRequest bridgeStatusInformationRequest;

	public BridgeStatusInformationRequest getBridgeStatusInformationRequest() {
		return bridgeStatusInformationRequest;
	}

	public void setBridgeStatusInformationRequest(BridgeStatusInformationRequest bridgeStatusInformationRequest) {
		this.bridgeStatusInformationRequest = bridgeStatusInformationRequest;
	}
		
}
