package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.DTSCONGESTIONPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.DTSCongestionService;

@Component(value = "DTSCONGESTIONPTImpl")
public class DTSCONGESTIONPTImpl implements DTSCONGESTIONPT{

	private static Logger logger = LoggerFactory.getLogger(DTSCONGESTIONPTImpl.class);
	
	@Autowired
	DTSCongestionService service;
	
	@Override
	public CongestionStatusResponse routeCongestionInformation(CongestionStatusRequest parameters) {
		
		logger.info("CALLED routeCongestionInformation ON bcDTS-CONGESTION");
		logger.info("START TIME OPERATION routeCongestionInformation ON bcDTS-CONGESTION: "+System.currentTimeMillis());	
		CongestionStatusResponse congestionStatusResponse = service.getRouteCongestionInformation(parameters);
		logger.info("END TIME OPERATION routeCongestionInformation ON bcDTS-CONGESTION: "+System.currentTimeMillis());			
		return congestionStatusResponse;
	}

}
