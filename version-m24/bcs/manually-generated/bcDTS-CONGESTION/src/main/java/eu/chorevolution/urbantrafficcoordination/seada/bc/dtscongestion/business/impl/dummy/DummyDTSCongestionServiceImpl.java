package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.RouteSegmentCongestionStatus;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.RouteSegmentCongestionStatusData;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.DTSCongestionService;

@Service
public class DummyDTSCongestionServiceImpl implements DTSCongestionService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSCongestionServiceImpl.class);
	
	@Override
	public CongestionStatusResponse getRouteCongestionInformation(CongestionStatusRequest parameters) {

		logger.info("CALLED getRouteCongestionInformation ON DUMMY bcDTS-CONGESTION");	
		CongestionStatusResponse congestionStatusResponse = new CongestionStatusResponse();
		RouteSegmentCongestionStatusData routeSegmentCongestionStatusData = new RouteSegmentCongestionStatusData();
		RouteSegmentCongestionStatus routeSegmentCongestionStatus = new RouteSegmentCongestionStatus();
		routeSegmentCongestionStatus.setCongestionLevel(1);
		routeSegmentCongestionStatusData.setRouteSegmentCongestionStatus(routeSegmentCongestionStatus);
		congestionStatusResponse.getRouteSegmentsCongestionStatus().add(routeSegmentCongestionStatusData);
		return congestionStatusResponse;
	}

}
