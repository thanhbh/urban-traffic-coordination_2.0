package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationResponse;

public interface DTSWeatherService {
	WeatherInformationResponse getWaypointWeatherInformation(WeatherInformationRequest parameters);
}
