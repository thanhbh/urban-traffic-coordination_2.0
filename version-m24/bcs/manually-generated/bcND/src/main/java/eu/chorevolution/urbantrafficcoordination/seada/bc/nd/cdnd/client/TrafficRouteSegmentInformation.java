
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for trafficRouteSegmentInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="trafficRouteSegmentInformation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="routeSegment" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeSegment"/&gt;
 *         &lt;element name="congestionLevel" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="weatherCondition" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}weatherCondition"/&gt;
 *         &lt;element name="extraDataWaypoints" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}extraDataWaypoints"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "trafficRouteSegmentInformation", propOrder = {
    "routeSegment",
    "congestionLevel",
    "weatherCondition",
    "extraDataWaypoints"
})
public class TrafficRouteSegmentInformation {

    @XmlElement(required = true)
    protected RouteSegment routeSegment;
    protected int congestionLevel;
    @XmlElement(required = true)
    protected WeatherCondition weatherCondition;
    @XmlElement(required = true)
    protected ExtraDataWaypoints extraDataWaypoints;

    /**
     * Gets the value of the routeSegment property.
     * 
     * @return
     *     possible object is
     *     {@link RouteSegment }
     *     
     */
    public RouteSegment getRouteSegment() {
        return routeSegment;
    }

    /**
     * Sets the value of the routeSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteSegment }
     *     
     */
    public void setRouteSegment(RouteSegment value) {
        this.routeSegment = value;
    }

    /**
     * Gets the value of the congestionLevel property.
     * 
     */
    public int getCongestionLevel() {
        return congestionLevel;
    }

    /**
     * Sets the value of the congestionLevel property.
     * 
     */
    public void setCongestionLevel(int value) {
        this.congestionLevel = value;
    }

    /**
     * Gets the value of the weatherCondition property.
     * 
     * @return
     *     possible object is
     *     {@link WeatherCondition }
     *     
     */
    public WeatherCondition getWeatherCondition() {
        return weatherCondition;
    }

    /**
     * Sets the value of the weatherCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeatherCondition }
     *     
     */
    public void setWeatherCondition(WeatherCondition value) {
        this.weatherCondition = value;
    }

    /**
     * Gets the value of the extraDataWaypoints property.
     * 
     * @return
     *     possible object is
     *     {@link ExtraDataWaypoints }
     *     
     */
    public ExtraDataWaypoints getExtraDataWaypoints() {
        return extraDataWaypoints;
    }

    /**
     * Sets the value of the extraDataWaypoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtraDataWaypoints }
     *     
     */
    public void setExtraDataWaypoints(ExtraDataWaypoints value) {
        this.extraDataWaypoints = value;
    }

}
