
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for closureStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="closureStatus"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isClosed" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="opensAtTimeMillis" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "closureStatus", propOrder = {
    "isClosed",
    "opensAtTimeMillis"
})
public class ClosureStatus {

    protected boolean isClosed;
    protected long opensAtTimeMillis;

    /**
     * Gets the value of the isClosed property.
     * 
     */
    public boolean isIsClosed() {
        return isClosed;
    }

    /**
     * Sets the value of the isClosed property.
     * 
     */
    public void setIsClosed(boolean value) {
        this.isClosed = value;
    }

    /**
     * Gets the value of the opensAtTimeMillis property.
     * 
     */
    public long getOpensAtTimeMillis() {
        return opensAtTimeMillis;
    }

    /**
     * Sets the value of the opensAtTimeMillis property.
     * 
     */
    public void setOpensAtTimeMillis(long value) {
        this.opensAtTimeMillis = value;
    }

}
