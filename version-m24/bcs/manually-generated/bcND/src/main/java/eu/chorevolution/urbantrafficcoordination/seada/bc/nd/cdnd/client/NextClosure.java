
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nextClosure complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nextClosure"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fromMillis" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="toMillis" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nextClosure", propOrder = {
    "fromMillis",
    "toMillis"
})
public class NextClosure {

    protected long fromMillis;
    protected long toMillis;

    /**
     * Gets the value of the fromMillis property.
     * 
     */
    public long getFromMillis() {
        return fromMillis;
    }

    /**
     * Sets the value of the fromMillis property.
     * 
     */
    public void setFromMillis(long value) {
        this.fromMillis = value;
    }

    /**
     * Gets the value of the toMillis property.
     * 
     */
    public long getToMillis() {
        return toMillis;
    }

    /**
     * Sets the value of the toMillis property.
     * 
     */
    public void setToMillis(long value) {
        this.toMillis = value;
    }

}
