
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for accidentInformationData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accidentInformationData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accidentInformation" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}accidentInformation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accidentInformationData", propOrder = {
    "accidentInformation"
})
public class AccidentInformationData {

    @XmlElement(required = true)
    protected AccidentInformation accidentInformation;

    /**
     * Gets the value of the accidentInformation property.
     * 
     * @return
     *     possible object is
     *     {@link AccidentInformation }
     *     
     */
    public AccidentInformation getAccidentInformation() {
        return accidentInformation;
    }

    /**
     * Sets the value of the accidentInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccidentInformation }
     *     
     */
    public void setAccidentInformation(AccidentInformation value) {
        this.accidentInformation = value;
    }

}
