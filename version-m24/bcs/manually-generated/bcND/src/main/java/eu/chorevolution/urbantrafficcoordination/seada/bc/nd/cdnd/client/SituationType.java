
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for situationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="situationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ferry"/&gt;
 *     &lt;enumeration value="obstacle"/&gt;
 *     &lt;enumeration value="accident"/&gt;
 *     &lt;enumeration value="restriction"/&gt;
 *     &lt;enumeration value="trafficinfo"/&gt;
 *     &lt;enumeration value="roadworks"/&gt;
 *     &lt;enumeration value="roadconditions"/&gt;
 *     &lt;enumeration value="other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "situationType")
@XmlEnum
public enum SituationType {

    @XmlEnumValue("ferry")
    FERRY("ferry"),
    @XmlEnumValue("obstacle")
    OBSTACLE("obstacle"),
    @XmlEnumValue("accident")
    ACCIDENT("accident"),
    @XmlEnumValue("restriction")
    RESTRICTION("restriction"),
    @XmlEnumValue("trafficinfo")
    TRAFFICINFO("trafficinfo"),
    @XmlEnumValue("roadworks")
    ROADWORKS("roadworks"),
    @XmlEnumValue("roadconditions")
    ROADCONDITIONS("roadconditions"),
    @XmlEnumValue("other")
    OTHER("other");
    private final String value;

    SituationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SituationType fromValue(String v) {
        for (SituationType c: SituationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
